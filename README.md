# Clay Printing and Extruding

mud machine liveth

![img](images/DSC02999.jpg)
![img](images/DSC03041.jpg)

Personally, working with ceramics is attractive because of the craftsmanship involved, and this machine's ambition does a bit of violence to that. On balance, I met ceramic artists who were excited by the possibilities it presented, and ceramics are *very real* stuff - i.e. a machine that does on-demand production of long lasting kitchenware, etc, made to order, is fun in a democratizing-the-means-to-production kind of way.

![video](video/claystack2018_machine_web.mp4)

## The Machine

This is the first time I've tried building [Roller Coaster Gantries](https://gitlab.cba.mit.edu/jakeread/rctgantries) with aluminum, and I learned a lot about it. First thing: it feels awesome, looks great, and approaches viable machine stiffness. Second thing: flexures that are flexures in HDPE [are not flexures in ALU](http://shrugguy.com). I should've known, but I was rushed. Next thing then is to do a tuned version of NEMA23 gantries for 1/4" ALU, I think, or the same for N17 and 3/16" - I'm thinking about the Zund, or our new waterjet, to manufacture these. At this stage, I'd also like to move the belt off of the axis... so, maybe some bigger spiral on machine axis design is coming. 

In any case, the machine's XY stages are a rip on [this project](https://gitlab.cba.mit.edu/jakeread/smallgantries) (a t-square type setup) and the Z-axis is a pretty vanilla linear axis with a big cantilvered 3DP-type bed. 

![img](images/DSC02828.jpg)
![img](images/DSC02820.jpg)

## Extrusion

The resounding conclusion from my time trying to push clay through a tube is that the best way to do it is to push it through the tube.

After seleuthing through the internet for a while for clay extrusion techniques, I had originally decided that a pressure-fed resevoir with an auger at the final stage was the way to go. While this might be more elegant in the long run, I wouldn't say it's worth the complexity.

'Bag Clay' - i.e. clay directly from the bag - can be quite soft as is. We found that with ~ 20% water added, we could push it through a 1/4" inside diameter tube having ~ 1m of length with only 30psi. We also found this to slump extremely quickly - our walls were about 70mm tall when they slumped in on themselves.

The other important note is that loading the clay into whatever tube you'll load into the machine is an important part of the process. I.E. bubbles are bad: we had to take care to putty-knife our slip into the tube without also putty-knifeing in air. Big bubbles are catastrophic and small ones are imperfections. So, some solution for loading bag clay into a tube without bubbles: desired. Pug mill loader. Machine loading machine. Etc.

## Next Time

 - leadscrew-type plunger to extrude.
 - swappable / multiple plunger tubes - make machine loading smooth
 - bed clips? 
 - easily swapped / fabbed nozzles also desireable 

![video](video/haystack2018_summary_web.mp4)
![video](video/haystack2018_woods59s_web.mp4)